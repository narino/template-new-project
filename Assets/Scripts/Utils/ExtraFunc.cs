﻿using System;
using System.Globalization;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using UnityEngine;

public static class ExtraFunc
{
    #region Static Var
    //public static bool IsDebug = Debug.isDebugBuild;
    //public static string DebugVer = Application.version + "-test";
    public static NumberFormatInfo NumFormat
    {
        get
        {
            NumberFormatInfo nfi = new CultureInfo("en-US", false).NumberFormat;
            nfi.NumberGroupSeparator = ".";
            nfi.NumberDecimalSeparator = ",";

            return nfi;
        }
    }
    #endregion

    public static T GetCopyOf<T>(this Component comp, T other) where T : Component
    {
        Type type = comp.GetType();
        if (type != other.GetType()) return null; // type mis-match
        BindingFlags flags = BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Default | BindingFlags.DeclaredOnly;
        PropertyInfo[] pinfos = type.GetProperties(flags);
        foreach (var pinfo in pinfos)
        {
            if (pinfo.CanWrite)
            {
                try
                {
                    pinfo.SetValue(comp, pinfo.GetValue(other, null), null);
                }
                catch { } // In case of NotImplementedException being thrown. For some reason specifying that exception didn't seem to catch it, so I didn't catch anything specific.
            }
        }
        FieldInfo[] finfos = type.GetFields(flags);
        foreach (var finfo in finfos)
        {
            finfo.SetValue(comp, finfo.GetValue(other));
        }
        return comp as T;
    }

    public static string UpperFirst(this string text)
    {
        text = text.Trim();

        if (string.IsNullOrEmpty(text))
        {
            return string.Empty;
        }

        char[] letters = text.ToCharArray();
        letters[0] = char.ToUpper(letters[0]);

        return new string(letters);
    }

    public static string UpperFirstWords(this string text)
    {
        text = text.Trim();

        if (string.IsNullOrEmpty(text))
        {
            return string.Empty;
        }

        string[] words = text.Split(' ');
        string newStr = "";
        foreach(string word in words)
        {
            char[] letters = word.ToCharArray();
            letters[0] = char.ToUpper(letters[0]);

            newStr += new string(letters) + " ";
        }
        
        return newStr.Trim();
    }

    public static string ConvertUnicode(this string text)
    {
        text = text.Trim();

        string newStr = Regex.Replace(
            text,
            @"\\u(?<Value>[a-zA-Z0-9]{4})",
            m => {
                return ((char)int.Parse(m.Groups["Value"].Value, NumberStyles.HexNumber)).ToString();
            });

        return newStr.Trim();
    }

    public static string CutNum(this long val)
    {
        string valTxt = val + "";

        if (valTxt.Length > 6 && valTxt.Length <= 9)
        {
            //1.000.000 (7) ~ 100.000.000 (9)
            decimal cut = Math.Round((decimal)(val / Math.Pow(10, 6)), 3);
            valTxt = cut.ToString(NumFormat) + " J";
        }
        else if (valTxt.Length > 9 && valTxt.Length <= 12)
        {
            //1.000.000.000 (10) ~ 100.000.000.000 (12)
            decimal cut = Math.Round((decimal)(val / Math.Pow(10, 9)), 3);
            valTxt = cut.ToString(NumFormat) + " M";
        }
        else if (valTxt.Length > 12)
        {
            //1.000.000.000.000 (13) ~ 100.000.000.000 (12)
            decimal cut = Math.Round((decimal)(val / Math.Pow(10, 12)), 3);
            valTxt = cut.ToString(NumFormat) + " T";
        }

        return valTxt;
    }
}

public class ConvertBase64
{
    public static string Encode(string plainText)
    {
        if (plainText == null)
        {
            return null;
        }

        var plainTextBytes = Encoding.UTF8.GetBytes(plainText);
        return Convert.ToBase64String(plainTextBytes);
    }

    public static string Decode(string base64EncodedData)
    {
        if (base64EncodedData == null)
        {
            return null;
        }

        var base64EncodedBytes = Convert.FromBase64String(base64EncodedData);
        return Encoding.UTF8.GetString(base64EncodedBytes);
    }

}