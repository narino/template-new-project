﻿using UnityEngine;
using System.Collections;
using Agate.Core.Managers;
using System.Collections.Generic;

namespace Agate.Managers
{
    public class SoundsManager : MonoBehaviour
    {
        private static SoundsManager _instance;
        public static SoundsManager Instance
        {
            get
            {
                if (_instance == null)
                {
                    GameObject container = PersistentObject.Instance.gameObject;
                    _instance = container.AddComponent<SoundsManager>();
                    _instance.Init();

                    DontDestroyOnLoad(_instance);
                }
                return _instance;
            }
        }

        #region AudioSources
        private AudioSource _bgmSource;
        private AudioSource _sfxSource;
        private AudioSource _loopSource;
        #endregion

        #region keys
        private string _bgmVolumeKey = "audio_bgm";
        private string _sfxVolumeKey = "audio_sfx";
        private string _muteKey = "audio_mute";
        #endregion

        #region getter
        private float _loopMultiplier = 0.30f;
        private float _bgmVolume;
        public float BgmVolume
        {
            get
            {
                return _bgmVolume;
            }
            set
            {
                _bgmVolume = value;
                _loopSource.volume = _bgmVolume;
                if (_loopSource.clip != null)
                {
                    _bgmSource.volume = _bgmVolume * _loopMultiplier;
                }
                else
                {
                    _bgmSource.volume = _bgmVolume;
                }
            }
        }

        public float SfxVolume
        {
            get
            {
                return _sfxSource.volume;
            }
            set
            {
                _sfxSource.volume = value;
            }
        }

        public bool Mute
        {
            get
            {
                return _sfxSource.mute;
            }
            set
            {
                //to playerpref
                _sfxSource.mute = value;
                _bgmSource.mute = value;
                _loopSource.mute = value;
            }
        }
        #endregion

        #region AudioPath + data
        private const string BgmResPath = "Sounds/BGM/";
        private const string SfxResPath = "Sounds/SFX/";
        private const string AmbResPath = "Sounds/Ambience/";

        private Dictionary<string, string> _bgmClips;
        private Dictionary<string, string> _sfxClips;
        private Dictionary<string, string> _ambienceClips;
        #endregion

        private bool _isInit = false;
        private BGM _currBgm = BGM.None;
        private Ambience _currAmb = Ambience.None;
        private float _fadeTime = 1f;

        public void Init()
        {
            if (_isInit)
            {
                return;
            }

            if (_bgmSource == null)
            {

                _bgmSource = gameObject.AddComponent<AudioSource>();
                _bgmSource.loop = true;
            }

            if (_loopSource == null)
            {
                _loopSource = gameObject.AddComponent<AudioSource>();
                _loopSource.volume = 1f;
                _loopSource.loop = true;
            }

            if (_sfxSource == null)
            {
                _sfxSource = gameObject.AddComponent<AudioSource>();
                _sfxSource.volume = 1f;
                _sfxSource.loop = false;
            }

            BgmVolume = PlayerPrefs.GetFloat(_bgmVolumeKey, 0.6f);
            SfxVolume = PlayerPrefs.GetFloat(_sfxVolumeKey, 1f);

            bool mute = PlayerPrefs.GetInt(_muteKey, 0) == 1;
            Mute = mute;

            _bgmSource.Play();
            _isInit = true;
            Debug.Log("SoundsManager Init");
        }

        public void SaveSoundSetting()
        {
            PlayerPrefs.SetInt(_muteKey, (Mute ? 1 : 0));
            PlayerPrefs.SetFloat(_bgmVolumeKey, BgmVolume);
            PlayerPrefs.SetFloat(_sfxVolumeKey, SfxVolume);
            PlayerPrefs.Save();
        }

        public void RegisterClip(TypeAudio type, string nameReg, string nameClip)
        {
            switch(type)
            {
                case TypeAudio.BGM:
                    if(_bgmClips == null)
                    {
                        _bgmClips = new Dictionary<string, string>();
                    }

                    _bgmClips.Add(nameReg, nameClip);
                    break;

                case TypeAudio.SFX:
                    if (_sfxClips == null)
                    {
                        _sfxClips = new Dictionary<string, string>();
                    }

                    _sfxClips.Add(nameReg, nameClip);
                    break;

                case TypeAudio.Ambience:
                    if (_ambienceClips == null)
                    {
                        _ambienceClips = new Dictionary<string, string>();
                    }

                    _ambienceClips.Add(nameReg, nameClip);
                    break;
            }
        }

        public void PlayOneShot(AudioClip clip)
        {
            _sfxSource.PlayOneShot(clip);
        }
        
        public void PlaySFX(SFX sfx)
        {
            string resName = "";
            if(_sfxClips.ContainsKey(sfx.ToString()))
            {
                resName = _sfxClips[sfx.ToString()];
            }
            AudioClip clip = Resources.Load<AudioClip>(SfxResPath + resName);
            PlayOneShot(clip);
        }
        
        public void SetBGM(BGM bgm)
        {
            if (bgm == _currBgm)
            {
                return;
            }

            string resName = "";
            if (_bgmClips.ContainsKey(bgm.ToString()))
            {
                resName = _bgmClips[bgm.ToString()];
            }
            AudioClip clip = Resources.Load<AudioClip>(BgmResPath + resName);

            _bgmSource.clip = clip;
            _bgmSource.Play();
            _currBgm = bgm;
        }
        
        public void StopBGM()
        {
            _bgmSource.Stop();
            _currBgm = BGM.None;
        }

        public void SetLoop(Ambience ambience)
        {
            if (ambience == _currAmb)
            {
                return;
            }

            string resName = "";
            if (_ambienceClips.ContainsKey(ambience.ToString()))
            {
                resName = _ambienceClips[ambience.ToString()];
            }
            AudioClip clip = Resources.Load<AudioClip>(AmbResPath + resName);

            _loopSource.clip = clip;
            if (clip != null)
            {
                StartCoroutine(FadeBgm(BgmVolume, BgmVolume * _loopMultiplier));
                _loopSource.Play();
            }
            else
            {
                StartCoroutine(FadeBgm(0, BgmVolume));
            }

            _currAmb = ambience;
        }

        IEnumerator FadeBgm(float loopVolume, float bgmVolume)
        {
            float currStep = 0;
            float loopStartVol = _loopSource.volume;
            float bgmStartVol = _bgmSource.volume;

            while (currStep < _fadeTime)
            {
                currStep += Time.deltaTime;

                _loopSource.volume = Mathf.Lerp(loopStartVol, loopVolume, currStep / _fadeTime);
                _bgmSource.volume = Mathf.Lerp(bgmStartVol, bgmVolume, currStep / _fadeTime);

                yield return null;
            }

            _loopSource.volume = loopVolume;
            _bgmSource.volume = bgmVolume;

        }
    }
    public enum TypeAudio
    {
        BGM,
        SFX,
        Ambience
    }

    //@TODO : Isi enum2 untuk file sound. jangan hapus bagian NONE
    public enum SFX
    {
        Button
    }

    public enum BGM
    {
        MainBGM,
        None = 255
    }

    public enum Ambience
    {
        None
    }
}