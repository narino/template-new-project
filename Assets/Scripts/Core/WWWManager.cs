﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine.Events;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using Agate.Core.Managers;

namespace Agate.Managers
{
    public class WWWManager : MonoBehaviour
    {
        private static WWWManager _instance;
        public static WWWManager Instance
        {
            get
            {
                if (_instance == null)
                {
                    GameObject container = PersistentObject.Instance.gameObject;
                    _instance = container.AddComponent<WWWManager>();
                    DontDestroyOnLoad(_instance);
                }
                return _instance;
            }
        }

        //private const string _rootUrl = "http://202.158.46.53/api/";
        private const string _rootUrl = "http://api.nabungsahamgo.com/";
        private const string _devUrl = "http://113.11.130.170/api/";

        public Boolean EncryptData = false;
        public Boolean SendDataAsJson = true;

        [SerializeField]
        private List<WWWJob> _jobs = new List<WWWJob>();

        public float RefreshTime = 30;
        public float Timeout = 2f;
        private bool _sendLock = false;

        public void SendJobImmediate(string url, string jsonString, UnityAction<JSONObject> onSuccess = null, UnityAction<string, bool> onFail = null, bool isDev = false)
        {
            string rootUrl = !isDev ? _rootUrl : _devUrl;
            StartCoroutine(SendRequestImmediate(rootUrl + url, jsonString, onSuccess, onFail));
        }

        public void SendJobImmediateFull(string url, string jsonString, UnityAction<JSONObject> onSuccess = null, UnityAction<string, bool> onFail = null)
        {
            StartCoroutine(SendRequestImmediate(url, jsonString, onSuccess, onFail));
        }

        public void ReadFile(string url, UnityAction<string> onSuccess, UnityAction<string, bool> onFail = null)
        {
            StartCoroutine(RequestFile(url, null, onSuccess, onFail));
        }

        public void ReadFile(string url, string jsonString, UnityAction<string> onSuccess, UnityAction<string, bool> onFail = null)
        {
            //karena udah pasti ada data yg dikirim, jadi tambah _rootUrl
            StartCoroutine(RequestFile(_rootUrl + url, jsonString, onSuccess, onFail));
        }

        public void PushJob(string url, string data)
        {
            _jobs.Add(new WWWJob(url, data));
            SaveJobsToFile();
            SendJob();
        }

        void SendJob()
        {
            if (_sendLock)
            {
                Debug.Log("Still sending...");
                return;
            }

            if (_jobs.Count > 0)
            {
                WWWJob job = _jobs[0];
                _sendLock = true;
                StartCoroutine(SendRequestImmediate(_rootUrl + job.Url, job.Data, OnSendSuccess, OnSendFail));
            }
            else
            {
                Debug.Log("No WWW Job now");
            }
        }

        void OnSendSuccess(JSONObject json)
        {
            //Debug.Log(json.Print());
            _sendLock = false;
            _jobs.RemoveAt(0);
            SaveJobsToFile();
            SendJob();
        }

        void OnSendFail(string txt, bool status)
        {
            Debug.Log(txt);
            _sendLock = false;
            Invoke("SendJob", RefreshTime);
        }

        private const string _jobKey = "jobs";

        void SaveJobsToFile()
        {
            byte[] data;

            using (var memoryStream = new MemoryStream())
            {
                using (var binaryStream = new BinaryWriter(memoryStream))
                {
                    int n = _jobs.Count;
                    binaryStream.Write(n);
                    Debug.Log("total jobs: " + n);
                    for (int i = 0; i < n; i++)
                    {
                        var job = _jobs[i];
                        binaryStream.Write(job.Url);
                        binaryStream.Write(job.Data);
                    }
                    binaryStream.Close();
                }
                data = memoryStream.ToArray();
                memoryStream.Close();
            }

#if UNITY_ANDROID || UNITY_IOS
            var path = Path.Combine(Application.persistentDataPath, _jobKey + ".dat");
            using (var file = File.CreateText(path))
            {
                file.Write(Convert.ToBase64String(data));
                file.Close();
            }
            Debug.Log("SAVE JOB ANDROID!");
#else
            PlayerPrefs.SetString(_jobKey, Convert.ToBase64String(data));
            Debug.Log("SAVE JOB NOT ANDROID!");

#endif
        }

        void LoadJobsFromFile()
        {
            byte[] buffer = null;
#if UNITY_ANDROID || UNITY_IOS
            var path = Path.Combine(Application.persistentDataPath, _jobKey + ".dat");
            if (!File.Exists(path))
            {
                return;
            }

            using (var file = File.OpenText(path))
            {
                buffer = Convert.FromBase64String(file.ReadToEnd());
                file.Close();
            }
            Debug.Log("LOAD JOB ANDROID/IOS!");
#else
            if (!PlayerPrefs.HasKey(_jobKey))
            {
                Debug.Log("Data key not found");
                return;
            }
            buffer = Convert.FromBase64String(PlayerPrefs.GetString(_jobKey));
            Debug.Log("LOAD JOB NOT ANDROID!");
#endif
            _jobs = new List<WWWJob>();
            if (buffer != null)
            {
                using (var memoryStream = new MemoryStream(buffer))
                {
                    using (var binaryStream = new BinaryReader(memoryStream))
                    {
                        try
                        {
                            int n = binaryStream.ReadInt32();
                            Debug.Log("total jobs: " + n);
                            for (int i = 0; i < n; i++)
                            {
                                string url = binaryStream.ReadString();
                                string data = binaryStream.ReadString();
                                _jobs.Add(new WWWJob(url, data));
                            }
                        }
                        catch (Exception ex)
                        {
                            Debug.Log(ex.Message);
                        }
                    }
                }
            }
        }

        IEnumerator SendRequestImmediate(string url, string jsonString = null, UnityAction<JSONObject> onComplete = null, UnityAction<string, bool> onError = null)
        {
            yield return new WaitForEndOfFrame();

            WWW www;
            if (jsonString != null)
            {
                WWWForm form = new WWWForm();

                if(SendDataAsJson)
                {
                    string encryptedString = jsonString;
                    if (EncryptData)
                    {
                        byte[] encrypted = Encrypt(jsonString);
                        encryptedString = Convert.ToBase64String(encrypted);
                    }

                    form.AddField("data", encryptedString);
                }
                else
                {
                    JSONObject jsonObj = new JSONObject(jsonString);
                    foreach (string key in jsonObj.keys)
                    {
                        form.AddField(key, jsonObj.GetField(key).str);
                    }
                }

                www = new WWW(url, form);
                //Debug.Log("sending data \nurl : " + url + "\nraw : " + jsonString + "\nenc : " + encryptedString);
                //Debug.Log("sending data \nurl : " + url + "\nraw : " + jsonString);
            }
            else
            {
                www = new WWW(url);
            }
            yield return www;

            if (!string.IsNullOrEmpty(www.error))
            {
                if (onError != null)
                {
                    Debug.Log("www error " + www.error);
                    //onError("www error " + url + " => " + www.error, true);

                    string err = www.error;
                    if(err.Contains("ETIMEDOUT") || err.Contains("ENETUNREACH"))
                    {
                        err = "Gagal Menghubungkan!!!\nMaaf koneksi Anda sedang tidak stabil atau tidak terhubung.";
                    }
                    onError(err, true);
                }
            }
            else
            {
                //Debug.Log("response " + www.text);
                //string decrypted = Decrypt(Convert.FromBase64String(www.text));
                string decrypted = www.text;
                if(EncryptData)
                {
                    decrypted = Decrypt(Convert.FromBase64String(www.text));
                    //Debug.Log("response " + decrypted);
                }
                
                if (string.IsNullOrEmpty(decrypted))
                {
                    if (onError != null)
                    {
                        onError("SendRequestImmediate returned no body text.", true);
                    }
                    yield break;
                }

                //cek escape
                //decrypted = decrypted.Replace("\\", "");

                JSONObject json = new JSONObject(decrypted);
                //Debug.Log((json == null) + "|" + json.Count);
                var statusObject = json.GetField("status");
                if (statusObject == null)
                {
                    if (onComplete != null)
                    {
                        onComplete(json);
                    }
                }
                else
                {
                    bool stat = bool.Parse(statusObject + "");
                    if(stat)
                    {
                        if (onComplete != null)
                        {
                            onComplete(json);
                        }
                    }
                    else if (onError != null)
                    {
                        var message = json.GetField("message");
                        //onError(url + " => " +
                        //    message != null
                        //        ? message.ToString()
                        //        : "SendRequestImmediate returned error with no error message.", false);
                        onError(message != null
                                ? message.ToString()
                                : "SendRequestImmediate returned error with no error message.", false);
                    }
                }
            }
        }

        IEnumerator RequestFile(string url, string jsonString, UnityAction <string> onComplete, UnityAction<string, bool> onError = null)
        {
            yield return new WaitForEndOfFrame();

            WWW www;
            if (jsonString != null)
            {
                WWWForm form = new WWWForm();

                if (SendDataAsJson)
                {
                    string encryptedString = jsonString;
                    if (EncryptData)
                    {
                        byte[] encrypted = Encrypt(jsonString);
                        encryptedString = Convert.ToBase64String(encrypted);
                    }

                    form.AddField("data", encryptedString);
                }
                else
                {
                    JSONObject jsonObj = new JSONObject(jsonString);
                    foreach (string key in jsonObj.keys)
                    {
                        form.AddField(key, jsonObj.GetField(key).str);
                    }
                }

                www = new WWW(url, form);
            }
            else
            {
                www = new WWW(url);
            }
            yield return www;

            if (!string.IsNullOrEmpty(www.error))
            {
                if (onError != null)
                {
                    Debug.Log("www error " + www.error);
                    onError("www error => " + www.error, true);
                }
            }
            else
            {
                JSONObject json = new JSONObject(www.text);
                if(json.HasField("status"))
                {
                    onError(json.GetField("message").str, true);
                }
                else
                {
                    onComplete(www.text);
                }
            }
        }

        #region Helper
        private const string EncryptKey = "l06ft4?q9v!s3i79bw9wifodokth6i13";
        private const string EncryptIv = "751952hhawyy66#cs!9hjv887mxx7@8x";

        public static byte[] Encrypt(string text)
        {
            byte[] encrypted;
            using (var rj = new RijndaelManaged())
            {
                rj.BlockSize = 256;
                rj.Padding = PaddingMode.Zeros;
                rj.Key = Encoding.ASCII.GetBytes(EncryptKey);
                rj.IV = Encoding.ASCII.GetBytes(EncryptIv);

                var encryptor = rj.CreateEncryptor(rj.Key, rj.IV);
                using (var msEncrypt = new MemoryStream())
                {
                    using (var csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                    {
                        using (var swEncrypt = new StreamWriter(csEncrypt))
                        {
                            swEncrypt.Write(text);
                        }

                        encrypted = msEncrypt.ToArray();
                    }
                }
            }

            return encrypted;
        }

        public static string Decrypt(byte[] bytes)
        {
            string decrypted;
            using (var rj = new RijndaelManaged())
            {
                rj.BlockSize = 256;
                rj.Padding = PaddingMode.None;
                rj.Key = Encoding.ASCII.GetBytes(EncryptKey);
                rj.IV = Encoding.ASCII.GetBytes(EncryptIv);

                var decryptor = rj.CreateDecryptor(rj.Key, rj.IV);
                using (var msDecrypt = new MemoryStream(bytes))
                {
                    using (var csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                    {
                        using (var srDecrypt = new StreamReader(csDecrypt))
                        {
                            decrypted = srDecrypt.ReadToEnd();
                        }
                    }
                }
            }

            return decrypted;
        }

        public static string GetHash(string input)
        {
            var sha = SHA1.Create();

            byte[] data = sha.ComputeHash(Encoding.ASCII.GetBytes(input));

            var sBuilder = new StringBuilder();
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            return sBuilder.ToString();
        }
        #endregion
    }

    [Serializable]
    public class WWWJob
    {
        public string Url;
        public string Data;

        public WWWJob(string url, string data)
        {
            Url = url;
            Data = data;
        }
    }
}