﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using PersistentObject = Agate.Core.Managers.PersistentObject;

namespace Agate.Core.Helper
{
    public class DelayHelper : MonoBehaviour
    {
        private static DelayHelper _instance;
        public static DelayHelper Instance
        {
            get
            {
                if (_instance == null)
                {
                    GameObject container = PersistentObject.Instance.gameObject;
                    _instance = container.AddComponent<DelayHelper>();
                    DontDestroyOnLoad(_instance);
                }
                return _instance;
            }
        }

        public static void DelayOneFrame(UnityAction action)
        {
            Instance.StartCoroutine(DelayOneFrameE(action));
        }


        static IEnumerator DelayOneFrameE(UnityAction action)
        {
            yield return null;
            action.Invoke();
        }

        public static void DelaySeconds(UnityAction action, float delay)
        {
            Instance.StartCoroutine(DelaySecondsE(action, delay));
        }

        static IEnumerator DelaySecondsE(UnityAction action, float delay)
        {
            yield return new WaitForSeconds(delay);
            action.Invoke();
        }
    }
}