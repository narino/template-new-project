﻿using UnityEngine;
using System.Collections;

namespace Agate.Core.Managers
{
    public class PersistentObject : MonoBehaviour
    {
        private static PersistentObject _instance;
        public static PersistentObject Instance
        {
            get
            {
                if(_instance == null)
                {
                    GameObject container = new GameObject("Persistent Object");
                    DontDestroyOnLoad(container);

                    _instance = container.AddComponent<PersistentObject>();
                    DontDestroyOnLoad(_instance);
                }
                return _instance;
            }
        }

        void Awake()
        {
            if(_instance == null)
            {
                _instance = this;
            }
            else
            {
                Destroy(this);
            }
        }
    }
}
